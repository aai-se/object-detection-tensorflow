# README #

This Package is designed to serve custom object detection models built on Tensorflow

### What is this repository for? ###

* Object Detection
* v0.1
* Requirements: A2019.10+

### How do I get set up? ###

* Import package in A2019
* Use the Session Start action to instantiate your custom tensorflow model (*.pb) and label file (*.txt)
* Use the Detect Objects action on an image to retrieve a List of Dictionaries. Each Dictionary represents a Detected Object
* Each dictionary has 6 keys: "score", "label", "x", "y", "height", "width"
* Those dictionaries can be used to crop the original image and extract content from individual objects

