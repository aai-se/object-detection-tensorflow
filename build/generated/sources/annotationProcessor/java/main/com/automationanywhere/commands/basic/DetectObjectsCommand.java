package com.automationanywhere.commands.basic;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.Boolean;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Number;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class DetectObjectsCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(DetectObjectsCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    DetectObjects command = new DetectObjects();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sessionName") && parameters.get("sessionName") != null && parameters.get("sessionName").get() != null) {
      convertedParameters.put("sessionName", parameters.get("sessionName").get());
      if(!(convertedParameters.get("sessionName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sessionName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.containsKey("InputFilePath") && parameters.get("InputFilePath") != null && parameters.get("InputFilePath").get() != null) {
      convertedParameters.put("InputFilePath", parameters.get("InputFilePath").get());
      if(!(convertedParameters.get("InputFilePath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","InputFilePath", "String", parameters.get("InputFilePath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("InputFilePath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","InputFilePath"));
    }

    if(parameters.containsKey("confidence") && parameters.get("confidence") != null && parameters.get("confidence").get() != null) {
      convertedParameters.put("confidence", parameters.get("confidence").get());
      if(!(convertedParameters.get("confidence") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","confidence", "Number", parameters.get("confidence").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("confidence") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","confidence"));
    }

    if(parameters.containsKey("GenerateOutputImage") && parameters.get("GenerateOutputImage") != null && parameters.get("GenerateOutputImage").get() != null) {
      convertedParameters.put("GenerateOutputImage", parameters.get("GenerateOutputImage").get());
      if(!(convertedParameters.get("GenerateOutputImage") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","GenerateOutputImage", "Boolean", parameters.get("GenerateOutputImage").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("GenerateOutputImage") != null && (Boolean)convertedParameters.get("GenerateOutputImage")) {
      if(parameters.containsKey("OutputFilePath") && parameters.get("OutputFilePath") != null && parameters.get("OutputFilePath").get() != null) {
        convertedParameters.put("OutputFilePath", parameters.get("OutputFilePath").get());
        if(!(convertedParameters.get("OutputFilePath") instanceof String)) {
          throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","OutputFilePath", "String", parameters.get("OutputFilePath").get().getClass().getSimpleName()));
        }
      }

      if(parameters.containsKey("BoundingBoxHexColor") && parameters.get("BoundingBoxHexColor") != null && parameters.get("BoundingBoxHexColor").get() != null) {
        convertedParameters.put("BoundingBoxHexColor", parameters.get("BoundingBoxHexColor").get());
        if(!(convertedParameters.get("BoundingBoxHexColor") instanceof String)) {
          throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","BoundingBoxHexColor", "String", parameters.get("BoundingBoxHexColor").get().getClass().getSimpleName()));
        }
      }

      if(parameters.containsKey("LineThickness") && parameters.get("LineThickness") != null && parameters.get("LineThickness").get() != null) {
        convertedParameters.put("LineThickness", parameters.get("LineThickness").get());
        if(!(convertedParameters.get("LineThickness") instanceof Number)) {
          throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","LineThickness", "Number", parameters.get("LineThickness").get().getClass().getSimpleName()));
        }
      }

    }

    command.setSessions(sessionMap);
    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("sessionName"),(String)convertedParameters.get("InputFilePath"),(Number)convertedParameters.get("confidence"),(Boolean)convertedParameters.get("GenerateOutputImage"),(String)convertedParameters.get("OutputFilePath"),(String)convertedParameters.get("BoundingBoxHexColor"),(Number)convertedParameters.get("LineThickness")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
