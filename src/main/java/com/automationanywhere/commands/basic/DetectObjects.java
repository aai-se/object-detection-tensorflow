package com.automationanywhere.commands.basic;


import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commands.misc.*;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.tensorflow.SavedModelBundle;
import org.tensorflow.Tensor;
import org.tensorflow.types.UInt8;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.NUMBER;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand
@CommandPkg(label="Detect Objects", name="Detect Objects", description="Detect Objects",  icon="",
        node_label="Detect Objects" ,
        return_type= DataType.LIST, return_sub_type=DataType.ANY, return_label="List of Dictionary Objects with 7 keys: 'page', 'label', 'score', 'x', 'y', 'width', 'height'", return_required=true)
public class DetectObjects {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");
    //private static final Logger logger = LogManager.getLogger(DetectObjects.class);

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public ListValue<DictionaryValue> action(@Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
                                             @Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Input Document (PDF or Image)", default_value_type = DataType.FILE) @NotEmpty String InputFilePath,
                                             @Idx(index = "3", type = AttributeType.NUMBER) @Pkg(label = "Confidence Threshold (%)", default_value_type = DataType.NUMBER) @NotEmpty Number confidence,
                                             @Idx(index = "4", type = AttributeType.CHECKBOX) @Pkg(label = "Generate Output Document", default_value_type = DataType.BOOLEAN, default_value = "false")  Boolean GenerateOutputImage,
                                             @Idx(index = "4.1", type = AttributeType.FILE) @Pkg(label = "Document with Printed Detections", default_value_type = DataType.FILE)  String OutputFilePath,
                                             @Idx(index = "4.2", type = TEXT) @Pkg(label = "Bounding Box Hex Color", default_value_type = STRING, default_value = "#f55d42",description = "ex: #f55d42")  String BoundingBoxHexColor,
                                             @Idx(index = "4.3", type = AttributeType.NUMBER) @Pkg(label = "Bounding Box Thickness", default_value_type = DataType.NUMBER, default_value = "4",description = "ex: 4")  Number LineThickness

    ) throws Exception {

        if("".equals(BoundingBoxHexColor)) {
            throw new BotCommandException(MESSAGES.getString("emptyInputString", "BoundingBoxHexColor"));
        }

        Boolean isHexString = BoundingBoxHexColor.matches("#[A-Za-z0-9]{6}");
        if(!isHexString) {
            throw new BotCommandException(MESSAGES.getString("wrongColorString", "BoundingBoxHexColor"));
        }

        File InputFile = new File(InputFilePath);
        if(! InputFile.exists()) {
            throw new BotCommandException(MESSAGES.getString("inputFileMissing",InputFilePath ));
        }

        ArrayList<DictionaryValue> AllDetectedObjects = new ArrayList<DictionaryValue>();

        // Loading Model
        TensorFlowSession tssession  = (TensorFlowSession) this.sessions.get(sessionName);
        SavedModelBundle model  = tssession.getModel();

        // Loading File Processor for multiple page pdfs
        FileProcessor fp = new FileProcessor(InputFilePath);

        ArrayList<String> AllImages = fp.GetFileAsImages();

        TFUtils tfutils = new TFUtils();

        if(fp.isFileAPdf()){
            // actual OpenCV Logic
            int pageNum = 1;
            for(String Image : AllImages){
               // Get Predictions for each
                File ImgFile = new File(Image);
                ArrayList<DictionaryValue> DetectedObjects = processSingleImage(ImgFile,pageNum,model,confidence,tssession);
                Image image = ImageIO.read(ImgFile);
                BufferedImage bufferimg = (BufferedImage) image;
                AllDetectedObjects.addAll(DetectedObjects);
                if(GenerateOutputImage){
                    BufferedImage imagenew = Utils.drawDetectedObjects(BoundingBoxHexColor,LineThickness,bufferimg, DetectedObjects);
                    //File outputfile = new File(OutputFilePath);
                    ImageIO.write(imagenew, "jpg", ImgFile);
                }
                pageNum++;
            }
            // if printing output:
            boolean res = fp.CombineImagesToPdf(AllImages,OutputFilePath);
            fp.Cleanup();


        }else{

            ArrayList<DictionaryValue> DetectedObjects = processSingleImage(InputFile,1,model,confidence,tssession);

            Image image = ImageIO.read(InputFile);
            BufferedImage bufferimg = (BufferedImage) image;
            AllDetectedObjects.addAll(DetectedObjects);


            if(GenerateOutputImage){
                if (OutputFilePath != null) {
                    BufferedImage imagenew = Utils.drawDetectedObjects(BoundingBoxHexColor,LineThickness,bufferimg, AllDetectedObjects);
                    File outputfile = new File(OutputFilePath);
                    ImageIO.write(imagenew, "jpg", outputfile);
                }
            }

        }

        ListValue AllObjects = new ListValue();
        AllObjects.set(AllDetectedObjects);

        return AllObjects;

    }

    public  ArrayList<DictionaryValue> processSingleImage(File InputFile,int PageNum,SavedModelBundle model,Number confidence,TensorFlowSession tssession) throws IOException {
        TFUtils tfutils = new TFUtils();
        java.util.List<Tensor<?>> AllTensors = tfutils.GetImageAs(InputFile,model);
        float confidenceValue = (float) ((confidence != null) ? (confidence.intValue()/100.0) : 0.5) ;
        List<DetectedObj> results = Utils.getDetections(AllTensors,tssession.getLabels(),confidenceValue);
        // ArrayList<DictionaryValue> AllDetectedObjectsAsDict = new ArrayList<DictionaryValue>();
        Image image = ImageIO.read(InputFile);
        BufferedImage bufferimg = (BufferedImage) image;
        ArrayList<DictionaryValue> AllDetectedObjects = tfutils.GetPredictionsFromImage(confidence,PageNum,AllTensors,tssession,bufferimg);
        return AllDetectedObjects;
    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }

}





