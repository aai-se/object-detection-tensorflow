package com.automationanywhere.commands.basic;


import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commands.misc.*;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.tensorflow.SavedModelBundle;
import org.tensorflow.Tensor;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand
@CommandPkg(label="Draw Objects", name="Draw Objects", description="Draw Objects",  icon="",
        node_label="Draw Objects" ,
        return_type= STRING, return_label="Status", return_required=true)
public class DrawObjects {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");
    //private static final Logger logger = LogManager.getLogger(DetectObjects.class);

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public StringValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Json Input from V8", default_value_type = STRING,  default_value = "Default") @NotEmpty String JsonInput,
            @Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Input Document (PDF or Image)", default_value_type = DataType.FILE) @NotEmpty String InputFilePath,
            @Idx(index = "3", type = AttributeType.FILE) @Pkg(label = "Output Document (PDF or Image)", default_value_type = DataType.FILE) @NotEmpty String OutputFilePath

    ) throws Exception {

        if("".equals(JsonInput)) {
            throw new BotCommandException(MESSAGES.getString("emptyInputString", "BoundingBoxHexColor"));
        }

        File InputFile = new File(InputFilePath);


        if(! InputFile.exists()) {
            throw new BotCommandException(MESSAGES.getString("inputFileMissing",InputFilePath ));
        }

        // Loading File Processor for multiple page pdfs
        FileProcessor fp = new FileProcessor(InputFilePath);
        //ArrayList<String> AllImages = fp.GetFileAsImages();

        if(fp.isFileAPdf()){
            // Not Supported Yet
            fp.Cleanup();
        }else{

            Image image = ImageIO.read(InputFile);
            BufferedImage BufferedInputFile = (BufferedImage) image;
            BufferedImage BufferedOutputFile = Utils.drawDetectedObjects(BufferedInputFile,JsonInput);

            int height = BufferedOutputFile.getHeight();
            System.out.println("Height:"+height);
            System.out.println("output:"+OutputFilePath);
            ImageIO.write(BufferedOutputFile, "png", new File(OutputFilePath));

        }

        return new StringValue("");

    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }

}





