package com.automationanywhere.commands.basic;

import com.automationanywhere.commands.misc.TensorFlowSession;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;

import java.io.File;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;


@BotCommand
@CommandPkg(label = "Session End", name = "Session End", description = "Session End",
        icon = "", node_label = "Session End {{sessionName}}|")

public class SessionEnd {

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public void end(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,
                    default_value = "Default") @NotEmpty String sessionName) {

        TensorFlowSession tssession  = (TensorFlowSession) this.sessions.get(sessionName);
        tssession.getModel().close();
        new File(tssession.getTempDir()+"/labels.txt").delete();
        new File(tssession.getTempDir()+"/saved_model.pb").delete();
        new File(tssession.getTempDir()).delete();

        sessions.remove(sessionName);

    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
