package com.automationanywhere.commands.basic;


import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commands.misc.TensorFlowSession;
import com.automationanywhere.commands.misc.Utils;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.tensorflow.SavedModelBundle;

import java.util.List;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand
@CommandPkg(label = "Session Start", name = "Session Start", description = "Session Start",
        icon = "", node_label = "Session Start {{sessionName}}|")
public class SessionStart {

    private static final Logger logger = LogManager.getLogger(SessionStart.class);

    @Sessions
    private Map<String, Object> sessions;

    private static final Messages MESSAGES = MessagesFactory
            .getMessages("com.automationanywhere.botcommand.demo.messages");

    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
    private GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(GlobalSessionContext globalSessionContext) {
        this.globalSessionContext = globalSessionContext;
    }

    @Execute
    public void start(@Idx(index = "1", type = TEXT) @Pkg(label = "Session name",  default_value_type = STRING, default_value = "Default") @NotEmpty String sessionName,
                      @Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Saved Model File",  default_value_type = DataType.FILE, default_value = "Default") @NotEmpty String modelfile,
                      @Idx(index = "3", type = AttributeType.FILE) @Pkg(label = "Label Text File",  default_value_type = DataType.FILE, default_value = "Default") @NotEmpty String labelfile ) throws Exception {

        // Check for existing session
        if (sessions.containsKey(sessionName))
            throw new BotCommandException(MESSAGES.getString("Session name in use ")) ;

        String TempModelDirectory = Utils.ExtractAndPrepModelFiles(modelfile,labelfile);
        List<String> labels = Utils.getLabels(TempModelDirectory);

        logger.info("Model Dir "+TempModelDirectory);

        SavedModelBundle model =  SavedModelBundle.load(TempModelDirectory,"serve");

        this.sessions.put(sessionName, new TensorFlowSession(model, TempModelDirectory,labels));

    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }

}