package com.automationanywhere.commands.misc;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import org.tensorflow.SavedModelBundle;
import org.tensorflow.Tensor;
import org.tensorflow.types.UInt8;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TFUtils {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    public static  java.util.List<Tensor<?>> GetImageAs(File InputFilePath, SavedModelBundle model){
        //Image image = ImageIO.read(new File(InputFilePath));
        Image image = null;
        try{
            image = ImageIO.read(InputFilePath);
            BufferedImage bufferimg = (BufferedImage) image;
            Tensor<UInt8> input = Utils.makeImageTensor(bufferimg);
            java.util.List<Tensor<?>> outputs = model
                    .session()
                    .runner()
                    .feed("image_tensor", input)
                    .fetch("detection_scores")
                    .fetch("detection_classes")
                    .fetch("detection_boxes")
                    .run();
            return outputs;
        }catch(IOException e){
            throw new BotCommandException(MESSAGES.getString("imageProcessingError",e.getMessage())) ;
        }
    }

    public static ArrayList<DictionaryValue> GetPredictionsFromImage(Number confidenceThreshold, Integer PageNumber,java.util.List<Tensor<?>> outputs,TensorFlowSession tssession,BufferedImage bufferimg){


        float confidenceValue = (float) ((confidenceThreshold != null) ? (confidenceThreshold.intValue()/100.0) : 0.5) ;

        List<DetectedObj> result = null;

        try {
            result = Utils.getDetections(outputs,tssession.getLabels(),confidenceValue);
        } catch (Exception e) {
            throw new BotCommandException(MESSAGES.getString("getPredictionError",e.getMessage())) ;
        }

        ArrayList<DictionaryValue> AllDetectedObjectsAsDict = new ArrayList<DictionaryValue>();

        for (DetectedObj detectedObj : result) {
            HashMap<String, Value> detectedObjMap = new HashMap<String,Value>();
            DictionaryValue ObjectDict = new DictionaryValue();
            Box BoundingBox = detectedObj.getBox();
            float X = BoundingBox.getTop();
            float Y = BoundingBox.getLeft();
            float Height = BoundingBox.getHeight();
            float Width = BoundingBox.getWidth();
            String Label = detectedObj.getLabel();
            float Score = detectedObj.getScore();

            String ReadablePercentagePrediction = Utils.ConvertScoreToHumanReadable(Score);

            int XInPixels = (int)(BoundingBox.getLeft() * bufferimg.getWidth());
            int YInPixels = (int)(BoundingBox.getTop() * bufferimg.getHeight());
            int widthInPixels = (int)(BoundingBox.getWidth() * bufferimg.getWidth());
            int heightInPixels = (int)(BoundingBox.getHeight() * bufferimg.getHeight());

            detectedObjMap.put("page",new StringValue(PageNumber.toString()));
            detectedObjMap.put("label",new StringValue(Label));
            detectedObjMap.put("score",new StringValue(ReadablePercentagePrediction));
            detectedObjMap.put("x",new StringValue(String.valueOf(XInPixels)));
            detectedObjMap.put("y",new StringValue(String.valueOf(YInPixels)));
            detectedObjMap.put("height",new StringValue(String.valueOf(heightInPixels)));
            detectedObjMap.put("width",new StringValue(String.valueOf(widthInPixels)));

            ObjectDict.set(detectedObjMap);
            AllDetectedObjectsAsDict.add(ObjectDict);
        }
        return AllDetectedObjectsAsDict;
    }

}
