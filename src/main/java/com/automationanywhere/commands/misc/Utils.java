package com.automationanywhere.commands.misc;


import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.tensorflow.Tensor;
import org.tensorflow.types.UInt8;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.json.simple.JSONArray;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Utils {

    public static String FileToString(String filePath)
    {
        StringBuilder contentBuilder = new StringBuilder();

        try (Stream<String> stream = Files.lines( Paths.get(filePath), StandardCharsets.UTF_8))
        {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return contentBuilder.toString();
    }

    public static BufferedImage drawDetectedObjects(BufferedImage img,String JsonInput) {

        JSONParser parser = new JSONParser();
        BufferedImage result = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
        //Graphics g = result.getGraphics();
        Graphics2D g = result.createGraphics();
        g.drawImage(img, 0, 0, null);

        try {
            JSONObject jobj = (JSONObject) parser.parse(JsonInput);
            JSONArray jarr = (JSONArray) jobj.get("KeyValueSet");

            for(int i =0;i<jarr.size();i++){
               JSONObject BlockObj= (JSONObject)jarr.get(i);
               String BlockType = (String) BlockObj.get("blockType");
               String BlockID = (String) BlockObj.get("id");
               String BlockText = (String) BlockObj.get("text");
               String BlockKeyType = (String) BlockObj.get("keyType");
               String BlockValue = (String) BlockObj.get("value");
               Double BlockConfidenceRetrieved;
               try{
                   BlockConfidenceRetrieved = (Double) BlockObj.get("confidence");
               }catch(ClassCastException e){
                    Long TempConf = (Long) BlockObj.get("confidence");
                   BlockConfidenceRetrieved = TempConf.doubleValue();
               }
                System.out.println("Debug:"+BlockConfidenceRetrieved);

               if(BlockConfidenceRetrieved<1.1){
                   BlockConfidenceRetrieved = BlockConfidenceRetrieved*100;
               }


               JSONObject BlockGeometry = (JSONObject)BlockObj.get("geometry");

               Double X1 = (Double) BlockGeometry.get("x1");
               Double Y1 = (Double) BlockGeometry.get("y1");
               Double X2 = (Double) BlockGeometry.get("x2");
               Double Y2 = (Double) BlockGeometry.get("y2");
                System.out.println("DEBUG: ID: "+BlockID+" - BlockType: "+BlockType+" - BlockKeyType: "+BlockKeyType+" - BlockValue: "+BlockValue+" - BlockConfidence: "+BlockConfidenceRetrieved+" - BlockText: "+BlockText);

               // int x = X1.intValue();
               // int y = Y1.intValue();
                //g.drawString(obj.getLabel(), x, y);
                int x0 = (int)(X1 * img.getWidth());
                int y0 = (int)(Y1 * img.getHeight());

                int width = (int)(X2 * img.getWidth()-x0);
                int height = (int)(Y2 * img.getHeight()-y0);
               // System.out.println("DRAWING:"+BlockText+"["+x0+":"+y0+"]["+width+":"+height+"]");
                g.setColor(hex2Rgb("#5eeb94"));
                g.drawRect(x0, y0, width+2, height+2);

                g.setColor(hex2Rgb("#f0f0f0"));
                //g.drawRect(x0-100, y0, 100, height+2);
                /*
                String BlockType = (String) BlockObj.get("blockType");
                String BlockID = (String) BlockObj.get("id");
                String BlockText = (String) BlockObj.get("text");
                String BlockKeyType = (String) BlockObj.get("keyType");
                String BlockValue = (String) BlockObj.get("value");
                Double BlockConfidence = (Double) BlockObj.get("confidence");
                */


                String Label = BlockType+" | "+BlockConfidenceRetrieved+"%"+" | "+BlockKeyType;
                int LabelRectLength = g.getFontMetrics().stringWidth(Label)+8;

                g.fillRect(x0-LabelRectLength, y0, LabelRectLength, height+2);


                Number Thickness = 2;
                float thickness = Thickness.floatValue();
                Stroke stroke = new BasicStroke(thickness);

                g.setStroke(stroke);
                g.setColor(hex2Rgb("#5ecaeb"));
                g.drawRect(x0-LabelRectLength, y0, LabelRectLength, height+2);
                g.drawString(Label, x0-LabelRectLength+4, y0+height-1);

            }

        } catch (ParseException e) {
            e.printStackTrace();
        }


/*
        for(DictionaryValue obj : objList){

            Map<String, Value> myMap = obj.get();

            String Label = myMap.get("label").toString();
            String X = myMap.get("x").toString();
            String Y = myMap.get("y").toString();
            String HEIGHT = myMap.get("height").toString();
            String WIDTH = myMap.get("width").toString();

            int iX=Integer.parseInt(X);
            int iY=Integer.parseInt(Y);
            int iHEIGHT=Integer.parseInt(HEIGHT);
            int iWIDTH=Integer.parseInt(WIDTH);

            g.setColor(Color.WHITE);

            int LabelRectLength = g.getFontMetrics().stringWidth(Label)+8;

            g.fillRect(iX+Thickness.intValue(), iY+Thickness.intValue(), LabelRectLength, 18);

            g.setColor(hex2Rgb(HexColor));
            float thickness = Thickness.floatValue();
            Stroke stroke = new BasicStroke(thickness);

            g.setStroke(stroke);

            g.drawString(Label, iX+3,iY+15);

            g.drawRect(iX,iY, iWIDTH, iHEIGHT);
        }
*/
        return result;
    }

    public static BufferedImage drawDetectedObjects(String HexColor,Number Thickness,BufferedImage img, ArrayList<DictionaryValue> objList) {

        BufferedImage result = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
        //Graphics g = result.getGraphics();
        Graphics2D g = result.createGraphics();
        g.drawImage(img, 0, 0, null);

        for(DictionaryValue obj : objList){

            Map<String, Value> myMap = obj.get();

            String Label = myMap.get("label").toString();
            String X = myMap.get("x").toString();
            String Y = myMap.get("y").toString();
            String HEIGHT = myMap.get("height").toString();
            String WIDTH = myMap.get("width").toString();

            int iX=Integer.parseInt(X);
            int iY=Integer.parseInt(Y);
            int iHEIGHT=Integer.parseInt(HEIGHT);
            int iWIDTH=Integer.parseInt(WIDTH);

            g.setColor(Color.WHITE);

            int LabelRectLength = g.getFontMetrics().stringWidth(Label)+8;

            g.fillRect(iX+Thickness.intValue(), iY+Thickness.intValue(), LabelRectLength, 18);

            g.setColor(hex2Rgb(HexColor));
            float thickness = Thickness.floatValue();
            Stroke stroke = new BasicStroke(thickness);

            g.setStroke(stroke);

            g.drawString(Label, iX+3,iY+15);

            g.drawRect(iX,iY, iWIDTH, iHEIGHT);
        }

        return result;
    }

    public static BufferedImage drawDetectedObjects(String HexColor,BufferedImage img, List<DetectedObj> objList) {

        BufferedImage result = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
        Graphics g = result.getGraphics();
        g.drawImage(img, 0, 0, null);

        //g.setColor(Color.yellow);
        g.setColor(hex2Rgb(HexColor));
        for(DetectedObj obj : objList){
            Box box = obj.getBox();
            int x = (int)(box.getLeft() * img.getWidth());
            int y = (int)(box.getTop() * img.getHeight());
            g.drawString(obj.getLabel(), x, y);
            int width = (int)(box.getWidth() * img.getWidth());
            int height = (int)(box.getHeight() * img.getHeight());
            g.drawRect(x, y, width, height);
        }

        return result;
    }

    public static Color hex2Rgb(String colorStr) {
        return new Color(
                Integer.valueOf( colorStr.substring( 1, 3 ), 16 ),
                Integer.valueOf( colorStr.substring( 3, 5 ), 16 ),
                Integer.valueOf( colorStr.substring( 5, 7 ), 16 ) );
    }


    public static Tensor<UInt8> makeImageTensor(BufferedImage img) throws IOException {

        byte[] data = ((DataBufferByte) img.getData().getDataBuffer()).getData();
        // ImageIO.read seems to produce BGR-encoded images, but the model expects RGB.
        bgr2rgb(data);
        final long BATCH_SIZE = 1;
        final long CHANNELS = 3;
        long[] shape = new long[] {BATCH_SIZE, img.getHeight(), img.getWidth(), CHANNELS};
        return Tensor.create(UInt8.class, shape, ByteBuffer.wrap(data));
    }

    private static void bgr2rgb(byte[] data) {
        for (int i = 0; i < data.length; i += 3) {
            byte tmp = data[i];
            data[i] = data[i + 2];
            data[i + 2] = tmp;
        }
    }

    public static  List<String>  getLabels(String dir) throws Exception, Exception {
        return Files.readAllLines(new File(dir+"/labels.txt").toPath());
    }

    public static List<DetectedObj> getDetections(List<Tensor<?>> outputs,List<String> labels,float confidence){
        List<DetectedObj> result = new ArrayList<>();

        float confidencelvl = (float) ((confidence != 0.0) ? confidence : 0.5) ;

            Tensor<Float> scoresT = outputs.get(0).expect(Float.class);
            Tensor<Float> classesT = outputs.get(1).expect(Float.class);
            Tensor<Float> boxesT = outputs.get(2).expect(Float.class);
            int maxObjects = (int) scoresT.shape()[1];

            float[] scores = scoresT.copyTo(new float[1][maxObjects])[0];
            float[] classes = classesT.copyTo(new float[1][maxObjects])[0];
            float[][] boxes = boxesT.copyTo(new float[1][maxObjects][4])[0];

            for (int i = 0; i < scores.length; ++i) {
                if (scores[i] < confidencelvl) {
                    continue;
                }
                float score = scores[i];
                float[] box = boxes[i];
                //System.out.println("DEBUG Retrieving Class Num:"+i);
                int LabelIDFromPred = (int) classes[i];
                String label = labels.get(LabelIDFromPred-1);

                float classeF = classes[i];

                DetectedObj detectedObj = new DetectedObj(label,score, box);
                result.add(detectedObj);

            }


        return result;
    }

    // For some reason Tensorflow requires hard coded file names for Models.. so yeha, let's do that
    public static String ExtractAndPrepModelFiles(String modelFile,String labelFile) throws Exception{

        Path pModelFile = Paths.get(modelFile);
        Path pLabelFile = Paths.get(labelFile);
        //String modelfileName = pModelFile.getFileName().toString();
        //String labelfileName = pLabelFile.getFileName().toString();
        String modelfileName = "saved_model.pb";
        String labelfileName = "labels.txt";

        // Create our temp file (first param is just random bits)
        Path path = Files.createTempDirectory("aats");
        File OriginalModelFile = new File(modelFile);
        File OriginalLabelFile = new File(labelFile);

        Files.copy(OriginalModelFile.toPath(), new File(path+"/"+modelfileName).toPath());
        Files.copy(OriginalLabelFile.toPath(), new File(path+"/"+labelfileName).toPath());


        return path.toString();

    }

    public static String ConvertScoreToHumanReadable(double RawConfidenceScore){
        RawConfidenceScore = RawConfidenceScore * 100;
        double RoundedScore = round(RawConfidenceScore, 2);
        return Double.toString(RoundedScore);
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
