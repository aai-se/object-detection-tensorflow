package com.automationanywhere.commands.tests;


import com.automationanywhere.commands.misc.Box;
import com.automationanywhere.commands.misc.DetectedObj;
import com.automationanywhere.commands.misc.Utils;
import org.tensorflow.SavedModelBundle;
import org.tensorflow.Tensor;
import org.tensorflow.types.UInt8;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/** Sample use of the TensorFlow Java API to label images using a pre-trained model. */
public class test1 {

    /* DETECTION */
    public static void main(String[] args) throws IOException {

        //String modelDir = "C:\\Users\\Administrator.EC2AMAZ-5L6MMDA\\Documents\\Tensorflow_models\\license_plates\\saved_model";
        String modelFile = "C:\\iqbot\\ObjectDetection\\Models\\license_plates\\saved_model.pb";
        String labelFile = "C:\\iqbot\\ObjectDetection\\Models\\license_plates\\labels.txt";

        //Path modelLabelList = Paths.get(modelDir,"labels.txt");

        String imageFile ="C:\\iqbot\\Document Samples\\Object_Detection\\license_plates\\pa7tl.jpg";
        String OutputImageFile = "C:\\iqbot\\Document Samples\\Object_Detection\\license_plates\\pa7tl_OUT.jpg";

        String TempModelFolder = null;
        List<String> labels = new ArrayList<String>();
        try {
            TempModelFolder = Utils.ExtractAndPrepModelFiles(modelFile,labelFile);
            labels = Utils.getLabels(TempModelFolder);
        } catch (Exception e) {
            e.printStackTrace();
        }

        SavedModelBundle model = SavedModelBundle.load(TempModelFolder,"serve");


        Image image = ImageIO.read(new File(imageFile));
        BufferedImage buffered = (BufferedImage) image;

        Tensor<UInt8> input = makeImageTensorINT(buffered);
        java.util.List<Tensor<?>> outputs = model
                .session()
                .runner()
                .feed("image_tensor", input)

                .fetch("detection_scores")
                .fetch("detection_classes")
                .fetch("detection_boxes")
                .run();

        Tensor<Float> output0 =   (Tensor<Float>)outputs.get(0);

        System.out.println("Dim"+output0.numDimensions());
        System.out.println("Size"+output0.shape().length);

        //java.util.List<String> labels = readAllLinesOrExit(modelLabelList);

        java.util.List<DetectedObj> result = new ArrayList<>();

        Tensor<Float> scoresT = outputs.get(0).expect(Float.class);
        Tensor<Float> classesT = outputs.get(1).expect(Float.class);
        Tensor<Float> boxesT = outputs.get(2).expect(Float.class) ;

        // All these tensors have:
        // - 1 as the first dimension
        // - maxObjects as the second dimension
        // While boxesT will have 4 as the third dimension (2 sets of (x, y) coordinates).
        // This can be verified by looking at scoresT.shape() etc.
        int maxObjects = (int) scoresT.shape()[1];
        float[] scores = scoresT.copyTo(new float[1][maxObjects])[0];
        float[] classes = classesT.copyTo(new float[1][maxObjects])[0];
        float[][] boxes = boxesT.copyTo(new float[1][maxObjects][4])[0];
        for (int i = 0; i < scores.length; ++i) {
            if (scores[i] < 0.1) {
                continue;
            }
            float score = scores[i];
            float[] box = boxes[i];
            System.out.println("DEBUG:"+labels.toString());
            String label = labels.get((int) classes[i])+" "+(int)(score*100)+"%";
            float classeF = classes[i];

            DetectedObj detectedObj = new DetectedObj(label,score, box);
            result.add(detectedObj);

            System.out.println("Class id "+classes[i]);

            System.out.println("Score "+score+" Class "+label);

            System.out.println("box"+box[0]+","+box[1]+","+box[2]+","+box[3]);

        }

        BufferedImage imagenew = drawDetectedObjects(buffered, result);

        File outputfile = new File(OutputImageFile);
        ImageIO.write(imagenew, "jpg", outputfile);

    }


    public static BufferedImage drawDetectedObjects(BufferedImage img, java.util.List<DetectedObj> objList) {

        BufferedImage result = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
        Graphics g = result.getGraphics();
        g.drawImage(img, 0, 0, null);

        g.setColor(Color.yellow);

        for(DetectedObj obj : objList){
            Box box = obj.getBox();
            int x = (int)(box.getLeft() * img.getWidth());
            int y = (int)(box.getTop() * img.getHeight());
            g.drawString(obj.getLabel(), x, y);
            int width = (int)(box.getWidth() * img.getWidth());
            int height = (int)(box.getHeight() * img.getHeight());
            g.drawRect(x, y, width, height);
        }

        return result;
    }

    public static Tensor<UInt8> makeImageTensorINT(BufferedImage img) throws IOException {

        byte[] data = ((DataBufferByte) img.getData().getDataBuffer()).getData();
        // ImageIO.read seems to produce BGR-encoded images, but the model expects RGB.
        bgr2rgb(data);
        final long BATCH_SIZE = 1;
        final long CHANNELS = 3;
        long[] shape = new long[] {BATCH_SIZE, img.getHeight(), img.getWidth(), CHANNELS};
        return Tensor.create(UInt8.class, shape, ByteBuffer.wrap(data));
    }

    private static void bgr2rgb(byte[] data) {
        for (int i = 0; i < data.length; i += 3) {
            byte tmp = data[i];
            data[i] = data[i + 2];
            data[i + 2] = tmp;
        }
    }

    private static List<String> readAllLinesOrExit(Path path) {
        try {
            return Files.readAllLines(path, Charset.forName("UTF-8"));
        } catch (IOException e) {
            System.err.println("Failed to read [" + path + "]: " + e.getMessage());
            System.exit(0);
        }
        return null;
    }


}

