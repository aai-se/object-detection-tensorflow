package com.automationanywhere.commands.tests;


import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.commands.basic.DetectObjects;
import com.automationanywhere.commands.misc.TensorFlowSession;
import com.automationanywhere.commands.misc.Utils;
import org.tensorflow.SavedModelBundle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Sample use of the TensorFlow Java API to label images using a pre-trained model. */
public class test2 {

    /* DETECTION */
    public static void main(String[] args) throws Exception {

        //String modelDir = "C:\\Users\\Administrator.EC2AMAZ-5L6MMDA\\Documents\\Tensorflow_models\\license_plates\\saved_model";
        String modelFile = "C:\\iqbot\\ObjectDetection\\Models\\license_plates\\saved_model.pb";
        String labelFile = "C:\\iqbot\\ObjectDetection\\Models\\license_plates\\labels.txt";

        //Path modelLabelList = Paths.get(modelDir,"labels.txt");

        String imageFile ="C:\\iqbot\\Document Samples\\Object_Detection\\license_plates\\pa7tl.jpg";
        String OutputImageFile = "C:\\iqbot\\Document Samples\\Object_Detection\\license_plates\\pa7tl_OUT.jpg";

        String TempModelFolder = null;
        List<String> labels = new ArrayList<String>();
        try {
            TempModelFolder = Utils.ExtractAndPrepModelFiles(modelFile,labelFile);
            labels = Utils.getLabels(TempModelFolder);
        } catch (Exception e) {
            e.printStackTrace();
        }

        SavedModelBundle model = SavedModelBundle.load(TempModelFolder,"serve");


        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put("Default",new TensorFlowSession(model, TempModelFolder,labels));

        DetectObjects command = new DetectObjects();
        command.setSessions(mso);

        ListValue d = command.action("Default",imageFile,50.0,true,OutputImageFile,"#f55d42",4 );
        List<DictionaryValue> dictList = d.get();

        System.out.println("Number of Objects Found: "+dictList.size());

        for(int i=0;i< dictList.size();i++){
            DictionaryValue dv = dictList.get(i);
            //System.out.println(dv.get().toString());

            Map<String, Value> myPredMap = dv.get();
            Value predClass = myPredMap.get("label");
            //System.out.println(predClass.toString());
            Value PredScore = myPredMap.get("score");
            //System.out.println(PredScore.toString());

            System.out.println(predClass.toString()+"|"+PredScore.toString());
        }




    }



}

