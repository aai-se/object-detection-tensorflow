package com.automationanywhere.commands.tests;


import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commands.basic.DetectObjects;
import com.automationanywhere.commands.basic.DrawObjects;
import com.automationanywhere.commands.misc.TensorFlowSession;
import com.automationanywhere.commands.misc.Utils;
import org.tensorflow.SavedModelBundle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Sample use of the TensorFlow Java API to label images using a pre-trained model. */
public class test5 {

    /* DETECTION */
    public static void main(String[] args) throws Exception {

        String imageFile ="C:\\iqbot\\doc1.png";
        String OutputImageFile = "C:\\iqbot\\doc1_out.png";
        String JsonInput = Utils.FileToString("c:\\iqbot\\v8_output_example.json");
        //String imageFile ="C:\\iqbot\\Invoice1.png";
        //String OutputImageFile = "C:\\iqbot\\Invoice1_out.png";
        //String JsonInput = Utils.FileToString("c:\\iqbot\\V8_output_2.json");
        DrawObjects command = new DrawObjects();


        StringValue s = command.action(JsonInput,imageFile,OutputImageFile);

    }



}

